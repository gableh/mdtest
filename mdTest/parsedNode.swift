//
// Created by Gable on 2019-07-06.
// Copyright (c) 2019 Gable. All rights reserved.
//

import Foundation

public class ParsedNode {
    private let attributedString: NSMutableAttributedString
    private let startLine: Int
    private let endLine: Int
    private let startColumn: Int
    private let endColumn: Int

    public init(attributedString: NSMutableAttributedString, startLine: Int, endLine: Int, startColumn: Int, endColumn: Int) {
        self.attributedString = attributedString
        self.startColumn = startColumn
        self.startLine = startLine
        self.endColumn = endColumn
        self.endLine = endLine
    }
}