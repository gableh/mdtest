//
//  ViewController.swift
//  mdTest
//
//  Created by Gable on 6/29/19.
//  Copyright © 2019 Gable. All rights reserved.
//

import UIKit
import Down

class ViewController: UIViewController, UITextViewDelegate {


    @IBOutlet weak var textView: UITextView!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        textView.delegate = self
        textView.allowsEditingTextAttributes = true
    }

    func splitAttributedString(inputString: NSAttributedString) -> [NSAttributedString] {
        let input = inputString.string
        let separatedInput = input.split { $0.isNewline }
        var output = [NSAttributedString]()
        var start = 0
        for sub in separatedInput {
            let range = NSMakeRange(start, sub.count)
            let attribStr = inputString.attributedSubstring(from: range)
            output.append(attribStr)
            start += range.length + "\n".count
        }
        return output
    }

    func textViewDidChange(_ textView: UITextView) {
        let selectedRange = textView.selectedRange;
        
        let down = Down(markdownString: textView.text)

        let asv: StringVisitor = StringVisitor(styler: CustomStyler(), textView: textView)
        let attributedStrings = asv.visitChildren(of: try! down.toAST(DownOptions(arrayLiteral: DownOptions(rawValue: 1 << 4),DownOptions(rawValue: 1 << 1))).wrap()!)
        textView.selectedRange = selectedRange
        let currentText: [NSMutableAttributedString] = textView.text.components(separatedBy: CharacterSet.newlines).map({string in
                return NSMutableAttributedString.init(string: string)
        })
        
        print("currentText")
        print(currentText)
        print("attributedStrings")
        print(attributedStrings)

        let newText = attributedStrings.reduce(NSMutableAttributedString(), { result, attributedString in
            if attributedString.string.isEmpty {
                return result
            }
            for index in (attributedString.attribute(NSAttributedString.Key("startLine"), at: 0, effectiveRange: nil) as! Int)-1...(attributedString.attribute(NSAttributedString.Key("endLine"), at: 0, effectiveRange: nil) as! Int)-1 {
                currentText[index].addAttributes(attributedString.attributes(at: 0, effectiveRange: nil), range: NSRange(location: 0, length: currentText[index].string.count))
            }
            return result
        })
        print("As")
        print(currentText)
        
        for (index, element) in currentText.enumerated() {
            if index != currentText.count-1 {
                element.mutableString.append("\n")
            }
        }
        
        let newAttributedText = currentText.reduce(NSMutableAttributedString(), { result, attributedString in
            print(attributedString.mutableString)
            print(attributedString.string)
            result.append(attributedString)
            
            return result
        })
        print(newAttributedText)
        textView.attributedText = newAttributedText
        textView.selectedRange = selectedRange
    }

    private class CustomStyler: Styler {
        var listPrefixAttributes: [NSAttributedString.Key: Any] = [:]

        func style(document str: NSMutableAttributedString) {
            str.addAttribute(.foregroundColor, value: UIColor.blue, range: NSRange(location: 0, length: str.mutableString.length))
        }

        func style(blockQuote str: NSMutableAttributedString) {
            str.addAttribute(.foregroundColor, value: UIColor.black, range: NSRange(location: 0, length: str.mutableString.length))
        }

        func style(list str: NSMutableAttributedString) {
            str.addAttribute(.foregroundColor, value: UIColor.brown, range: NSRange(location: 0, length: str.mutableString.length))
        }

        func style(item str: NSMutableAttributedString) {
            str.addAttribute(.foregroundColor, value: UIColor.clear, range: NSRange(location: 0, length: str.mutableString.length))
        }


        func style(codeBlock str: NSMutableAttributedString, fenceInfo: String?) {
            str.addAttribute(.foregroundColor, value: UIColor.cyan, range: NSRange(location: 0, length: str.mutableString.length))
        }

        func style(htmlBlock str: NSMutableAttributedString) {
            str.addAttribute(.foregroundColor, value: UIColor.darkGray, range: NSRange(location: 0, length: str.mutableString.length))
        }

        func style(customBlock str: NSMutableAttributedString) {
            str.addAttribute(.foregroundColor, value: UIColor.gray, range: NSRange(location: 0, length: str.mutableString.length))
        }

        func style(paragraph str: NSMutableAttributedString) {
            str.addAttribute(.foregroundColor, value: UIColor.darkText, range: NSRange(location: 0, length: str.mutableString.length))
        }

        func style(heading str: NSMutableAttributedString, level: Int) {
            str.addAttribute(.foregroundColor, value: UIColor.green, range: NSRange(location: 0, length: str.mutableString.length))
        }

        func style(thematicBreak str: NSMutableAttributedString) {
            str.addAttribute(.foregroundColor, value: UIColor.lightGray, range: NSRange(location: 0, length: str.mutableString.length))
        }

        func style(text str: NSMutableAttributedString) {
            str.addAttribute(.foregroundColor, value: UIColor.lightText, range: NSRange(location: 0, length: str.mutableString.length))

        }

        func style(softBreak str: NSMutableAttributedString) {
            str.addAttribute(.foregroundColor, value: UIColor.magenta, range: NSRange(location: 0, length: str.mutableString.length))
        }

        func style(lineBreak str: NSMutableAttributedString) {
            str.addAttribute(.foregroundColor, value: UIColor.orange, range: NSRange(location: 0, length: str.mutableString.length))
        }

        func style(code str: NSMutableAttributedString) {
            str.addAttribute(.foregroundColor, value: UIColor.purple, range: NSRange(location: 0, length: str.mutableString.length))
        }

        func style(htmlInline str: NSMutableAttributedString) {
            str.addAttribute(.foregroundColor, value: UIColor.white, range: NSRange(location: 0, length: str.mutableString.length))
        }

        func style(customInline str: NSMutableAttributedString) {
            str.addAttribute(.foregroundColor, value: UIColor.yellow, range: NSRange(location: 0, length: str.mutableString.length))
        }

        func style(emphasis str: NSMutableAttributedString) {
            str.addAttribute(.foregroundColor, value: UIColor.magenta, range: NSRange(location: 0, length: str.mutableString.length))
        }

        func style(strong str: NSMutableAttributedString) {
            str.addAttribute(.foregroundColor, value: UIColor.red, range: NSRange(location: 0, length: str.mutableString.length))
        }

        func style(link str: NSMutableAttributedString, title: String?, url: String?) {
            str.addAttribute(.foregroundColor, value: UIColor.red, range: NSRange(location: 0, length: str.mutableString.length))
        }

        func style(image str: NSMutableAttributedString, title: String?, url: String?) {
            str.addAttribute(.foregroundColor, value: UIColor.red, range: NSRange(location: 0, length: str.mutableString.length))
        }
    }
}

